"use strict";

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.sequelize.transaction(transaction =>
            Promise.all([
                queryInterface.addColumn(
                    "posts",
                    "deleted",
                    {
                        type: Sequelize.BOOLEAN,
                        defaultValue: false
                    },
                    { transaction }
                )
            ])
        ),

    down: queryInterface =>
        queryInterface.sequelize.transaction(transaction =>
            Promise.all([
                queryInterface.removeColumn("posts", "deleted", { transaction })
            ])
        )
};
