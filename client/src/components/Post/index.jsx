import React from "react";
import PropTypes from "prop-types";
import { Card, Image, Label, Icon } from "semantic-ui-react";
import moment from "moment";

import styles from "./styles.module.scss";

const Post = ({
    post,
    reactPost,
    toggleExpandedPost,
    sharePost,
    editPost,
    deletePost,
    restorePost,
    isPersonal
}) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt,
        deleted
    } = post;

    const date = moment(createdAt).fromNow();
    return (
        <Card style={{ width: "100%" }} className={deleted ? "deleted" : ""}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by {user.username}
                        {" - "}
                        {date}
                    </span>
                    {isPersonal && (
                        <div className="right floated">
                            {deleted ? (
                                <Label
                                    basic
                                    size="small"
                                    as="a"
                                    className={styles.toolbarBtn}
                                    onClick={() => restorePost(post)}
                                >
                                    Restore
                                    <Icon name="undo" />
                                </Label>
                            ) : (
                                <>
                                    <Label
                                        basic
                                        size="small"
                                        as="a"
                                        className={styles.toolbarBtn}
                                        onClick={() => editPost(post)}
                                    >
                                        <Icon name="edit" />
                                    </Label>
                                    <Label
                                        basic
                                        size="small"
                                        as="a"
                                        className={styles.toolbarBtn}
                                        onClick={() => deletePost(post)}
                                    >
                                        <Icon name="delete" />
                                    </Label>
                                </>
                            )}
                        </div>
                    )}
                </Card.Meta>
                <Card.Description>{body}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label
                    active={!deleted}
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() =>
                        !deleted && reactPost({ postId: id, isLike: true })
                    }
                >
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label
                    active={!deleted}
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() =>
                        !deleted && reactPost({ postId: id, isLike: false })
                    }
                >
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label
                    active={!deleted}
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => !deleted && toggleExpandedPost(id)}
                >
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label
                    active={!deleted}
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => !deleted && sharePost(id)}
                >
                    <Icon name="share alternate" />
                </Label>
            </Card.Content>
        </Card>
    );
};

Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    reactPost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

export default Post;
