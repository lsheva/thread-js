import React from "react";
import PropTypes from "prop-types";
import { Modal, Input, Icon, Form, Button } from "semantic-ui-react";

class DeletePost extends React.Component {
    render() {
        const { post, close } = this.props;
        return (
            <Modal open onClose={close}>
                <Modal.Header>
                    <span>Delete Post</span>
                </Modal.Header>
                <Modal.Content>
                    Are you sure want to delete this post?
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        color="red"
                        onClick={() => this.props.onSubmit(post)}
                    >
                        Delete
                    </Button>
                    <Button color="grey" onClick={close}>
                        Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

DeletePost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    close: PropTypes.func.isRequired
};

export default DeletePost;
