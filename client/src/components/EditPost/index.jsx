import React from "react";
import PropTypes from "prop-types";
import { Modal, Input, Icon, Form, Button } from "semantic-ui-react";

import styles from "./styles.module.scss";

class EditPost extends React.Component {
    state = {
        body: this.props.post.body,
        imageId: undefined,
        imageLink: undefined
    };

    render() {
        const { post, close } = this.props;
        const { body, imageId, imageLink } = this.state;
        return (
            <Modal open onClose={close}>
                <Modal.Header className={styles.header}>
                    <span>Edit Post</span>
                </Modal.Header>
                <Modal.Content>
                    <Form
                        onSubmit={() => this.props.onSubmit({ ...post, body })}
                    >
                        <Form.TextArea
                            name="body"
                            value={body}
                            onChange={e =>
                                this.setState({ body: e.target.value })
                            }
                        />
                        {/*{imageLink && (
                        <div className={styles.imageWrapper}>
                            <Image className={styles.image} src={imageLink} alt="post" />
                        </div>
                    )}
                     <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                        <Icon name="image" />
                        Attach image
                        <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                    </Button> */}
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="blue" type="submit">
                        Submit
                    </Button>
                    <Button color="grey" onClick={close}>
                        Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

EditPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    close: PropTypes.func.isRequired
};

export default EditPost;
