import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as imageService from "src/services/imageService";
import ExpandedPost from "src/containers/ExpandedPost";
import Post from "src/components/Post";
import AddPost from "src/components/AddPost";
import SharedPostLink from "src/components/SharedPostLink";
import EditPost from "src/components/EditPost";
import DeletePost from "src/components/DeletePost";
import { Checkbox, Loader } from "semantic-ui-react";
import InfiniteScroll from "react-infinite-scroller";
import {
    loadPosts,
    loadMorePosts,
    reactPost,
    editPost,
    deletePost,
    restorePost,
    toggleExpandedPost,
    addPost
} from "./actions";

import styles from "./styles.module.scss";

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            editPostObj: undefined,
            deletePostObj: undefined
        };
        this.postsFilter = {
            userId: undefined,
            deleted: false,
            from: 0,
            count: 10
        };
    }

    tooglePosts = () => {
        this.setState(
            ({ showOwnPosts }) => ({ showOwnPosts: !showOwnPosts }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showOwnPosts
                        ? this.props.userId
                        : undefined,
                    deleted: true,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    sharePost = sharedPostId => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    };

    editPost = editPostObj => {
        this.setState({ editPostObj });
    };

    closeEditPost = () => {
        this.setState({ editPostObj: undefined });
    };

    submitEditPost = async editPostObj => {
        await this.props.editPost(editPostObj);
        this.closeEditPost();
    };

    deletePost = deletePostObj => {
        this.setState({ deletePostObj });
    };
    closeDeletePost = () => {
        this.setState({ deletePostObj: undefined });
    };
    submitDeletePost = async deletePostObj => {
        await this.props.deletePost(deletePostObj);
        this.closeDeletePost();
    };
    restorePost = restorePostObj => {
        this.props.restorePost(restorePostObj);
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, hasMorePosts, ...props } = this.props;
        const {
            showOwnPosts,
            sharedPostId,
            editPostObj,
            deletePostObj
        } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost
                        addPost={props.addPost}
                        uploadImage={this.uploadImage}
                    />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox
                        toggle
                        label="Show only my posts"
                        checked={showOwnPosts}
                        onChange={this.tooglePosts}
                    />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            reactPost={props.reactPost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            editPost={this.editPost}
                            deletePost={this.deletePost}
                            restorePost={this.restorePost}
                            isPersonal={props.userId === post.userId}
                            key={post.id}
                        />
                    ))}
                </InfiniteScroll>
                {expandedPost && <ExpandedPost sharePost={this.sharePost} />}
                {sharedPostId && (
                    <SharedPostLink
                        postId={sharedPostId}
                        close={this.closeSharePost}
                    />
                )}
                {editPostObj && (
                    <EditPost
                        post={editPostObj}
                        close={this.closeEditPost}
                        onSubmit={this.submitEditPost}
                    />
                )}
                {deletePostObj && (
                    <DeletePost
                        post={deletePostObj}
                        close={this.closeDeletePost}
                        onSubmit={this.submitDeletePost}
                    />
                )}
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    editPostObj: PropTypes.objectOf(PropTypes.any),
    deletePostObj: PropTypes.objectOf(PropTypes.any),
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    reactPost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    editPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    reactPost,
    editPost,
    deletePost,
    restorePost,
    toggleExpandedPost,
    addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
